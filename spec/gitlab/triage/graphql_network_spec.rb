require 'spec_helper'

require 'gitlab/triage/options'
require 'gitlab/triage/graphql_network'

describe Gitlab::Triage::GraphqlNetwork do
  let(:token) { 'token' }
  let(:network_adapter) { double(options: network_options, parse: :parsed_query) }
  let(:query_builder) { double(query: :graphql_query, resource_path: [:projects, :merge_request]) }

  let(:network_options) do
    options = Gitlab::Triage::Options.new
    options.token = token
    options
  end

  let(:network) { described_class.new(network_adapter) }

  before do
    allow(network).to receive(:print)
  end

  describe '#query' do
    let(:first_response) do
      {
        end_cursor: 'END_CURSOR',
        more_pages: true,
        results: [
          {
            'id' => 'gid://gitlab/MergeRequest/1002',
            'userNotesCount' => 10
          }
        ],
        ratelimit_remaining: 600,
        ratelimit_reset_at: 1604942574
      }
    end

    let(:second_response) do
      {
        more_pages: false,
        results: [
          {
            'id' => 'gid://gitlab/MergeRequest/1003',
            'userNotesCount' => 50
          }
        ],
        ratelimit_reset_at: 599,
        ratelimit_remaining: 1604942574
      }
    end

    subject(:query) { network.query(query_builder, source: 'gitlab-org') }

    before do
      allow(network_adapter).to receive(:query).and_return(first_response, second_response)
    end

    it 'parses query with adapter' do
      expect(network_adapter).to receive(:parse).with(:graphql_query)
      query
    end

    it 'sends multiple requests to GraphQL network adapter to fetch all resources' do
      expect(network_adapter).to receive(:query)
        .with(:parsed_query, resource_path: [:projects, :merge_request], variables: { source: 'gitlab-org', after: nil })
      expect(network_adapter).to receive(:query)
        .with(:parsed_query, resource_path: [:projects, :merge_request], variables: { source: 'gitlab-org', after: 'END_CURSOR' })
      query
    end

    it 'returns array with underscore hashes with id extracted from global id' do
      expect(query).to eq([{ 'id' => 1002, 'user_notes_count' => 10 }, { 'id' => 1003, 'user_notes_count' => 50 }])
    end
  end
end
